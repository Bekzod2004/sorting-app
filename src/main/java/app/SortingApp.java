package app;

import java.util.Arrays;

public class SortingApp {
    /**
     *  This main method sorts the array of integers which is parsed from String array inserted by Command Line arguments
     * @param args String array Given by command-line arguments.
     * @throws IllegalArgumentException if there is invalid array length such as ZERO , ONE or more than TEN elements
    */
    public static void main(String[] args) {
        int len = args.length;
        int[] array = new int[len];
        try {
            for (int i = 0; i < len; i++)
                array[i] = Integer.parseInt(args[i]);

            if (len == 0)
                throw new IllegalArgumentException("Given array length is not valid because of ZERO element.");

            if (len == 1)
                throw new IllegalArgumentException("Given array length is not valid  to sort because of only ONE element.");

            if (len > 10)
                throw new IllegalArgumentException("Given array length is not valid because of more then TEN element. There should be less then eleven elements");

            sortAndPrint(array, len);

        } catch (NumberFormatException e) {
            System.err.println("Unable to parse the given value to integer - " + e.getMessage() + " and this is not considered as real number");
        }
    }

    private static void sortAndPrint(int[] arr, int len) {
        int count;
        do {
            count = 0;
            for (int i = 0; i < len - 1; i++)
                if (arr[i] > arr[i + 1]) {
                    arr[i] = arr[i] + arr[i + 1];
                    arr[i + 1] = arr[i] - arr[i + 1];
                    arr[i] = arr[i] - arr[i + 1];
                    count++;
                }
            len--;
        } while (count != 0);
        System.out.println(Arrays.toString(arr));
    }
}