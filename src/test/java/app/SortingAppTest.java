package app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import static org.junit.Assert.*;

//@RunWith(Parameterized.class)
public class SortingAppTest {

    @Test(expected = IllegalArgumentException.class)
    public void testZeroCase(){
        SortingApp.main(new String[0]);
    }


    @Test(expected = IllegalArgumentException.class)
    public void testOneCase(){
        SortingApp.main(new String[]{"1"});
    }


    @Test
    public void testValidCase(){
        String[] args = {"1","4","6","2","5"};
        SortingApp.main(args);
    }
}
