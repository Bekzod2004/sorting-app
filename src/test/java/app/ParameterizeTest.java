package app;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

@RunWith(Parameterized.class)
public class ParameterizeTest {
    private final String[] array;

    @Parameters
    public static List<Object[]> data() {
        return Arrays.asList(
                new Object[]{new String[]{"58", "41", "390", "390", "192", "24", "17"}},
                new Object[]{new String[]{"31", "134", "93", "619", "12", "78"}},
                new Object[]{new String[]{"12", "24", "43", "119", "32", "97"}},
                new Object[]{new String[]{"17", "42", "35", "109", "82", "71"}},
                new Object[]{new String[]{"75", "19", "37", "22", "55", "49"}},
                new Object[]{new String[]{"2", "7", "11", "1", "6"}}
        );
    }

    public ParameterizeTest(String[] array) {
        this.array = array;
    }

    @Test
    public void testParametrized() {
        assertTrue(true);
        try {
            SortingApp.main(array);
        } catch (Exception e) {
            fail();
        }
    }

}

